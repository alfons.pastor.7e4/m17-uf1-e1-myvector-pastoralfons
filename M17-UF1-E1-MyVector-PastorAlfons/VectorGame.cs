﻿using System;

namespace M17_UF1_E1_MyVector_PastorAlfons
{
    class VectorGame
    {
        public MyVector[] randomVectors(int arrayLength, double minX = -100, double minY = -100, double maxX = 100, double maxY = 100)
        {
            MyVector[] randomVectorArray = new MyVector[arrayLength];
            Random rand = new Random();
            double Ax, Ay, Bx, By;
            for (int i = 0; i < randomVectorArray.Length; i++)
            {
                Ax = rand.NextDouble() * (maxX - minX) + minX;
                Ay = rand.NextDouble() * (maxY - minY) + minY;
                Bx = rand.NextDouble() * (maxX - minX) + minX;
                By = rand.NextDouble() * (maxY - minY) + minY;
                randomVectorArray[i] = new MyVector(new Vector2D(Ax, Ay), new Vector2D(Bx, By));
            }
            return randomVectorArray;
        }
        public MyVector[] SortVectors(MyVector[] vectors, bool byMagnitude = true)
        {
            MyVectorComparer comparer = new MyVectorComparer(byMagnitude);
            Array.Sort(vectors, comparer);
            return vectors;
        }
    }
}
