﻿using System;

namespace M17_UF1_E1_MyVector_PastorAlfons
{
    class Program
    {
        static void Main(string[] args)
        {
            HelloName();
            MyVectorDemo();
            VectorGameDemo();
        }
        public static void HelloName()
        {
            string name;
            Console.WriteLine("What's your name?");
            name = Console.ReadLine();
            Console.WriteLine("Hello {0}\n", name);
        }
        public static void MyVectorDemo()
        {
            MyVector vec = new MyVector(new Vector2D(1, 2), new Vector2D(3, 5));
            //o amb l'altre constructor
            vec = new MyVector();
            vec.InitialPoint = new Vector2D(1, 2);
            vec.TerminalPoint = new Vector2D(3, 5);

            Console.WriteLine(vec); //ToString. Coordenades, longitud, distancia a l'origen
            double angle = 225;
            vec.SetVectorDirection(angle); //passem un angle (en graus)
            Console.WriteLine("\nVector rotated to {0} degrees: ", angle);
            Console.WriteLine(vec); //noves coordenades, conserva el modul i el punt inicial
        }
        public static void VectorGameDemo()
        {
            VectorGame vectorGame = new VectorGame();
            MyVector[] randomVectors = vectorGame.randomVectors(5); //fem 5 vectors aleatoris
            Console.WriteLine("\nRandom Vectors");
            PrintVectors(randomVectors);

            MyVector[] sortedVectors = vectorGame.SortVectors(randomVectors, true); //ordenem els vectors pel modul/longitud
            Console.WriteLine("\nSorted by magnitude");
            PrintVectors(sortedVectors);

            sortedVectors = vectorGame.SortVectors(randomVectors, false); //ordenem els vectors per la distancia a l'origen (0,0)"
            Console.WriteLine("\nSorted by distance to (0,0)");
            PrintVectors(sortedVectors);
        }
        public static void PrintVectors(MyVector[] vectors)
        {
            for (int i = 0; i < vectors.Length; i++)
            {
                Console.WriteLine(vectors[i]);
            }
        }
    }
}