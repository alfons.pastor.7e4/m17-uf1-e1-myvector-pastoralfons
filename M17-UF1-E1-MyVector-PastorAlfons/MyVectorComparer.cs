﻿using System.Collections.Generic;

namespace M17_UF1_E1_MyVector_PastorAlfons
{
    class MyVectorComparer : IComparer<MyVector>
    {
        private bool byMagnitude = true;
        public MyVectorComparer(bool byMagnitude = true)
        {
            this.byMagnitude = byMagnitude;
        }
        public int Compare(MyVector a, MyVector b)
        {
            switch (byMagnitude)
            {
                case true:
                    if (a.Length() > b.Length())
                    {
                        return 1;
                    }
                    else if (a.Length() < b.Length())
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }

                case false:
                    if (a.DistanceFromOrigin() > b.DistanceFromOrigin())
                    {
                        return 1;
                    }
                    else if (a.DistanceFromOrigin() < b.DistanceFromOrigin())
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
            }
        }
    }
}
