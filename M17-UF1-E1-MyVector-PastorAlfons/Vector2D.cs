﻿using System;

namespace M17_UF1_E1_MyVector_PastorAlfons
{
    //free-form vectors
    class Vector2D
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Vector2D()
        {
            X = 0;
            Y = 0;
        }
        public Vector2D(double x, double y)
        {
            X = x;
            Y = y;
        }
        public double Magnitude()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }
        public override string ToString()
        {
            return string.Format("({0:N2}, {1:N2})", X, Y);
        }
        public static Vector2D operator +(Vector2D a) => a;
        public static Vector2D operator -(Vector2D a) => new Vector2D(-a.X, -a.Y);
        public static Vector2D operator +(Vector2D a, Vector2D b)
            => new Vector2D(a.X + b.X, a.Y + b.Y);

        public static Vector2D operator -(Vector2D a, Vector2D b)
            => a + (-b);
    }
}
