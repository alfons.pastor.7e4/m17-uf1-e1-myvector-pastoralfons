﻿using System;

namespace M17_UF1_E1_MyVector_PastorAlfons
{
    //class representing euclidean vectors / directed line segments
    class MyVector
    {
        private Vector2D[] coordinates;
        public Vector2D[] Coordinates
        {
            get => coordinates;
            set
            {
                if (value.Length != 2)
                {
                    throw new ArgumentException("Wrong array size. Size 2 is expected.");
                }
                else
                {
                    coordinates = value;
                }
            }

        }
        public Vector2D InitialPoint
        {
            get => Coordinates[0];
            set
            {
                Coordinates[0] = value;
            }
        }
        public Vector2D TerminalPoint
        {
            get => Coordinates[1];
            set
            {
                Coordinates[1] = value;
            }
        }
        public MyVector()
        {
            Coordinates = new Vector2D[2];
            InitialPoint = new Vector2D(0, 0);
            TerminalPoint = new Vector2D(0, 0);
        }
        public MyVector(Vector2D initialPoint, Vector2D terminalPoint)
        {
            Coordinates = new Vector2D[2];
            InitialPoint = initialPoint;
            TerminalPoint = terminalPoint;
        }
        public double Length()
        {
            return (TerminalPoint - InitialPoint).Magnitude();
        }

        //Distance from the initial point, or the terminal point, whichever is closer, to the origin of the coordinate system 
        public double DistanceFromOrigin()
        {
            if (InitialPoint.Magnitude() < TerminalPoint.Magnitude())
            {
                return InitialPoint.Magnitude();
            }
            else
            {
                return TerminalPoint.Magnitude();
            }
        }
        //Sets the direction (angle in degrees) of the vector. Magnitude and initial point remain unchanged.
        //Frame of reference: 0º points east. Rotates counterclockwise (180º points west)
        public void SetVectorDirection(double angle)
        {
            angle = (angle * Math.PI) / 180;
            Vector2D freeRotated2DVector = new Vector2D(Length() * Math.Cos(angle), Length() * Math.Sin(angle));
            TerminalPoint = new Vector2D(freeRotated2DVector.X + InitialPoint.X, freeRotated2DVector.Y + InitialPoint.Y);
        }
        public override string ToString()
        {
            return string.Format("A: {0}   B: {1}   Magnitude: {2:N2}   Distance from origin: {3:N2}", InitialPoint, TerminalPoint, Length(), DistanceFromOrigin());
        }
    }
}
